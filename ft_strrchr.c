/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/12 21:54:37 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/14 12:46:35 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*beg;
	char	uch;

	beg = (char *)s;
	uch = (char)c;
	i = ft_strlen(s);
	if (uch == '\0')
		return (beg + i);
	while (&beg[i] != &s[0])
	{
		if (beg[i] == uch)
		{
			return (&beg[i]);
		}
		i--;
	}
	if (s[0] == uch)
		return (beg);
	return (NULL);
}
