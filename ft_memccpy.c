/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 11:55:22 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/01 14:07:12 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t		i;
	char		*string;
	char		*strings;

	string = (char *)dst;
	strings = (char *)src;
	i = 0;
	while (i < n)
	{
		string[i] = strings[i];
		if (strings[i] == c)
		{
			return (string + i + 1);
		}
		i++;
	}
	return (NULL);
}
