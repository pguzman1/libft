/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 10:56:25 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/07 16:05:25 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned int	i;
	char			*string1;
	char			*string2;

	string1 = (char *)dst;
	string2 = (char *)src;
	i = 0;
	while (i < n)
	{
		string1[i] = string2[i];
		i++;
	}
	return (dst);
}
