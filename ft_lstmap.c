/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 10:07:23 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/14 13:16:08 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*temp;
	t_list	*res;
	t_list	*elem;

	res = ft_lstnew(lst->content, lst->content_size);
	if (res == NULL)
		return (NULL);
	res->content = f(lst)->content;
	res->content_size = f(lst)->content_size;
	temp = res;
	lst = lst->next;
	while (lst)
	{
		elem = ft_lstnew(lst->content, lst->content_size);
		if (elem == NULL)
			return (NULL);
		elem->content = f(lst)->content;
		elem->content_size = f(lst)->content_size;
		temp->next = elem;
		lst = lst->next;
		temp = temp->next;
	}
	return (res);
}
