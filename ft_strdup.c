/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 11:15:16 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/10 15:18:23 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strdup(const char *s1)
{
	char	*temp;

	temp = (char *)malloc((ft_strlen(s1) + 1) * sizeof(*temp));
	if (temp == NULL)
		return (NULL);
	ft_memcpy(temp, s1, ft_strlen(s1) + 1);
	return (temp);
}
